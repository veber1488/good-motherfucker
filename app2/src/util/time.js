import { format } from 'date-fns'

export default function install (Vue) {
  Object.defineProperties(Vue.prototype, {
    $time: format,
  })
}
