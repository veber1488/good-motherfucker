import axios from 'axios'

const
  apiUrl = 'http://api2.osr.com.ua/api/v1/',
  csrfElement = document.getElementsByName('csrf-token')[0],
  csrfToken = csrfElement && csrfElement.getAttribute('content') || '',
  http = axios.create({
    baseURL: apiUrl,
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      common: {
        'X-CSRF-TOKEN': csrfToken,
        Authorization: 'Bearer E6O5CtBWRMqugtplufYBStOJgcxQA3MUCIWmndS5',
      },
    },
  })

/**
 * Helper method to set the header with the token
 */
// export function setToken (token) {
//   http.defaults.headers.common.Authorization = 'Bearer '
// }

// receive store and data by options
// https://vuejs.org/v2/guide/plugins.html
export default function install (Vue) {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty
  Object.defineProperty(Vue.prototype, '$http', {
    get () {
      return http
    },
  })
}
