export default {
  computed: {
    isExact () {
      let matched = this.$route.matched
      return matched && matched.length
        ? matched[matched.length - 1].name === this.routeName
        : false
    },
  },
}
