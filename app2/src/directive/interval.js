export default (Vue) => {
  Vue.directive('select', {
    bind () {
      var self = this
      $(this.el).SumoSelect({
        placeholder: $(this.el).data('placeholder'),
        data: this.params.options
      })
      $(this.el).on('change', function (evt) {
        self.set($(evt.target).val())
      })
    },
    unbind () {
      if ($(this.el)[0].sumo !== undefined) {
        $(this.el)[0].sumo.unload()
      }
    }
  })
}
