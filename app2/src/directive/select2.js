export default (Vue) => {
  Vue.directive('select', {
    bind (el, binding, vnode) {
      let
        vm = this,
        $el = $(el)

      $el.SumoSelect({
        placeholder: $el.data('placeholder'),
        data: this.params.options,
      })

      $el.on('change', (e) => {
        vm.set(e.target.value)
      })
    },

    update (el, binding, vnode) {
      let $el = $(el)

      $el.val(binding.value).trigger('change')

      if (typeof $el[0].sumo !== 'undefined') {
        $el[0].sumo.selectItem(binding.value)
      }
    },

    unbind (el, binding, vnode) {
      let $el = $(el)

      if (typeof $el[0].sumo !== 'undefined') {
        $el[0].sumo.unload()
      }
    },
  })
}
