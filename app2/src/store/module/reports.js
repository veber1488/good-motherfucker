import Vue from 'vue'
import {
  REPORT_LOAD,
  REPORT_UNLOAD,
  REPORT_UPDATE,
  REPORTS_PUSH,
  REPORTS_PULL,
  REPORTS_UPDATE,
} from '../mutation-types'

const
  state = {
    isLoaded: false,
    reports: [],
    report: null,
  },

  mutations = {
    [REPORT_LOAD] (state, report) {
      Vue.set(state, 'report', report)
    },

    [REPORT_UNLOAD] (state) {
      Vue.set(state, 'report', null)
    },

    [REPORT_UPDATE] (state, report) {
      let idx = state.reports.findIndex(item => report.id === item.id)
      if (idx > 0) {
        Vue.set(state.reports, idx, report)
        if (state.report && state.report.id === report.id) {
          Vue.set(state, 'report', report)
        }
      } else {
        Vue.set(state.reports, state.reports.length, report)
      }
    },

    [REPORTS_PUSH] (state, report) {
      Vue.set(state.reports, state.reports.length, report)
    },

    [REPORTS_PULL] (state, report) {
      state.reports.splice(state.reports.findIndex(item => report.id === item.id), 1)
      if (state.report && state.report.id === report.id) {
        Vue.set(state, 'report', null)
      }
    },

    [REPORTS_UPDATE] (state, reports) {
      Vue.set(state, 'reports', reports)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    REPORT_LOAD ({ commit }, report) {
      commit(REPORT_LOAD, report)
    },
    REPORT_UNLOAD ({ commit }) {
      commit(REPORT_UNLOAD)
    },
    REPORT_UPDATE ({ commit }, report) {
      commit(REPORT_UPDATE, report)
    },
    REPORTS_PUSH ({ commit }, report) {
      commit(REPORTS_PUSH, report)
    },
    REPORTS_PULL ({ commit }, report) {
      commit(REPORTS_PULL, report)
    },
    REPORTS_UPDATE ({ commit }, reports) {
      commit(REPORTS_UPDATE, reports)
    },
  },

  getters = {
    reportsLoaded: state => state.isLoaded,
    getReports: state => state.reports,
    getReport: state => state.report,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
