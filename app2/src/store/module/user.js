import Vue from 'vue'
import {
  USER_LOAD,
  USER_UNLOAD,
  TOKEN_UPDATE,
} from '../mutation-types'

const
  state = {
    isLoaded: false,
    user: null,
    token: null,
  },

  mutations = {
    [USER_LOAD] (state, user) {
      Vue.set(state, 'user', user)
    },

    [USER_UNLOAD] (state) {
      Vue.set(state, 'user', null)
    },

    [TOKEN_UPDATE] (state, token) {
      Vue.set(state, 'token', token)
    },
  },

  actions = {
    USER_LOAD ({ commit }, user) {
      commit(USER_LOAD, user)
    },
    USER_UNLOAD ({ commit }) {
      commit(USER_UNLOAD)
    },
    TOKEN_UPDATE ({ commit }, user) {
      commit(TOKEN_UPDATE, user)
    },
  },

  getters = {
    isGuest: state => !state.isLoaded,
    getToken: state => state.token,
    getUser: state => state.user,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
