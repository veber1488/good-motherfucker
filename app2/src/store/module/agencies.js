import Vue from 'vue'
import {
  AGENCY_LOAD,
  AGENCY_UNLOAD,
  AGENCY_UPDATE,
  AGENCIES_PUSH,
  AGENCIES_PULL,
  AGENCIES_UPDATE,
} from '../mutation-types'

const
  state = {
    isLoaded: false,
    agencies: [],
    agency: null,
  },

  mutations = {
    [AGENCY_LOAD] (state, agency) {
      Vue.set(state, 'agency', agency)
    },

    [AGENCY_UNLOAD] (state) {
      Vue.set(state, 'agency', null)
    },

    [AGENCY_UPDATE] (state, agency) {
      let idx = state.agencies.findIndex(item => agency.id === item.id)
      if (idx > 0) {
        Vue.set(state.agencies, idx, agency)
        if (state.agency && state.agency.id === agency.id) {
          Vue.set(state, 'agency', agency)
        }
      } else {
        Vue.set(state.agencies, state.agencies.length, agency)
      }
    },

    [AGENCIES_PUSH] (state, agency) {
      Vue.set(state.agencies, state.agencies.length, agency)
    },

    [AGENCIES_PULL] (state, agency) {
      state.agencies.splice(state.agencies.findIndex(item => agency.id === item.id), 1)
      if (state.agency && state.agency.id === agency.id) {
        Vue.set(state, 'agency', null)
      }
    },

    [AGENCIES_UPDATE] (state, agencies) {
      Vue.set(state, 'agencies', agencies)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    AGENCY_LOAD ({ commit }, agency) {
      commit(AGENCY_LOAD, agency)
    },
    AGENCY_UNLOAD ({ commit }) {
      commit(AGENCY_UNLOAD)
    },
    AGENCY_UPDATE ({ commit }, agency) {
      commit(AGENCY_UPDATE, agency)
    },
    AGENCIES_PUSH ({ commit }, agency) {
      commit(AGENCIES_PUSH, agency)
    },
    AGENCIES_PULL ({ commit }, agency) {
      commit(AGENCIES_PULL, agency)
    },
    AGENCIES_UPDATE ({ commit }, agencies) {
      commit(AGENCIES_UPDATE, agencies)
    },
  },

  getters = {
    agenciesLoaded: state => state.isLoaded,
    getAgencies: state => state.agencies,
    getAgency: state => state.agency,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
