import Vue from 'vue'
import {
  DISTRICT_LOAD,
  DISTRICT_UNLOAD,
  DISTRICT_UPDATE,
  DISTRICTS_PUSH,
  DISTRICTS_PULL,
  DISTRICTS_UPDATE,
} from '../mutation-types'

const
  state = {
    isLoaded: false,
    districts: [],
    district: null,
  },

  mutations = {
    [DISTRICT_LOAD] (state, district) {
      Vue.set(state, 'district', district)
    },

    [DISTRICT_UNLOAD] (state) {
      Vue.set(state, 'district', null)
    },

    [DISTRICT_UPDATE] (state, district) {
      let idx = state.districts.findIndex(item => district.id === item.id)
      if (idx > 0) {
        Vue.set(state.districts, idx, district)
        if (state.district && state.district.id === district.id) {
          Vue.set(state, 'district', district)
        }
      } else {
        Vue.set(state.districts, state.districts.length, district)
      }
    },

    [DISTRICTS_PUSH] (state, district) {
      Vue.set(state.districts, state.districts.length, district)
    },

    [DISTRICTS_PULL] (state, district) {
      state.districts.splice(state.districts.findIndex(item => district.id === item.id), 1)
      if (state.district && state.district.id === district.id) {
        Vue.set(state, 'district', null)
      }
    },

    [DISTRICTS_UPDATE] (state, districts) {
      Vue.set(state, 'districts', districts)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    DISTRICT_LOAD ({ commit }, district) {
      commit(DISTRICT_LOAD, district)
    },
    DISTRICT_UNLOAD ({ commit }) {
      commit(DISTRICT_UNLOAD)
    },
    DISTRICT_UPDATE ({ commit }, district) {
      commit(DISTRICT_UPDATE, district)
    },
    DISTRICTS_PUSH ({ commit }, district) {
      commit(DISTRICTS_PUSH, district)
    },
    DISTRICTS_PULL ({ commit }, district) {
      commit(DISTRICTS_PULL, district)
    },
    DISTRICTS_UPDATE ({ commit }, districts) {
      commit(DISTRICTS_UPDATE, districts)
    },
  },

  getters = {
    districtsLoaded: state => state.isLoaded,
    getDistricts: state => state.districts,
    getDistrict: state => state.district,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
