import user from './module/user'
import cities from './module/cities'
import shares from './module/shares'
import streets from './module/streets'
import objects from './module/objects'
import reports from './module/reports'
import agencies from './module/agencies'
import districts from './module/districts'
import notifications from './module/notifications'

export default {
  user,
  cities,
  shares,
  streets,
  objects,
  reports,
  agencies,
  districts,
  notifications,
}
