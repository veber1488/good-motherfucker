import Vue from 'vue'
import { SET_LOADING } from './mutation-types'

export default {
  [SET_LOADING] (state, value) {
    Vue.set(state, 'loading', value)
  },
}
