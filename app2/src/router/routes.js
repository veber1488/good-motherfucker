export default [
  {
    path: '/',
    component: () => import('../pages/DashboardPage.vue'),
    name: 'home',
    redirect: '/objects',
    meta: { requiresAuth: true },
    children: [
      {
        path: 'objects',
        name: 'objects',
        redirect: '/objects/sale',
        component: () => import('../pages/ObjectsPage.vue'),
        children: [
          {
            path: 'sale',
            name: 'objects.sale',
            component: () => import('../pages/SalePage.vue'),
          },
          {
            path: 'rent',
            name: 'objects.rent',
            component: () => import('../pages/RentPage.vue'),
            children: [
              {
                path: 'shorttime',
                name: 'objects.rent.shorttime',
                component: () => import('../pages/ShorttimePage.vue'),
              },
              {
                path: 'longtime',
                name: 'objects.rent.longtime',
                component: () => import('../pages/LongtimePage.vue'),
              },
            ],
          },
          {
            path: 'add',
            name: 'objects.add',
            component: () => import('../pages/ObjectAddPage.vue'),
            meta: {
              allowedRoles: ['manager', 'admin'],
              hideFilters: true,
            },
          },
          {
            path: 'edit/:id',
            name: 'objects.edit',
            component: () => import('../pages/ObjectAddPage.vue'),
            meta: {
              allowedRoles: ['manager', 'admin'],
              hideFilters: true,
            },
          },
        ],
      },
      {
        path: 'office',
        name: 'office',
        component: () => import('../pages/OfficePage.vue'),
        children: [
          {
            path: 'bids',
            name: 'office.bids',
            component: () => import('../pages/BidsPage.vue'),
            children: [
              {
                path: 'view/:id',
                name: 'office.bids.view',
                component: () => import('../pages/BidPage.vue'),
              },
              {
                path: 'edit/:id',
                name: 'office.bids.edit',
                component: () => import('../pages/BidEditPage.vue'),
                meta: { allowedRoles: ['manager', 'admin'] },
              },
            ],
          },
          {
            path: 'notifications',
            name: 'office.notifications',
            component: () => import('../pages/NotificationsPage.vue'),
            children: [
              {
                path: 'view/:id',
                name: 'office.notifications.view',
                component: () => import('../pages/NotificationPage.vue'),
              },
              {
                path: 'edit/:id',
                name: 'office.notifications.edit',
                component: () => import('../pages/NotificationEditPage.vue'),
                meta: { allowedRoles: ['manager', 'admin'] },
              },
            ],
          },
        ],
      },
    ],
  },
  {
    path: '/login',
    component: () => import('../pages/LoginPage.vue'),
    name: 'login',
  },
  {
    path: '/register',
    component: () => import('../pages/RegisterPage.vue'),
    name: 'register',
  },
]
