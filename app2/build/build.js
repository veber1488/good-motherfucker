// https://github.com/shelljs/shelljs
require('shelljs/global')

const
  // config = require('../config'),
  ora = require('ora'),
  webpack = require('webpack'),
  webpackConfig = require('./webpack.prod.conf'),
  fs = require('fs-extra'),
  chalk = require('chalk'),
  spinner = ora('building for production...')

spinner.start()

// empty build folder because webpack-cleanup-plugin doesn't remove folders
fs.emptyDirSync(webpackConfig.output.path)

webpack(webpackConfig, function (err, stats) {
  spinner.stop()
  if (err) throw err
  process.stdout.write(stats.toString({
    colors: true,
    modules: true,
    children: true,
    chunks: true,
    chunkModules: true,
    reasons: true
  }) + '\n')

  console.log()
  console.log(chalk.blue('You can preview your build by running:'), chalk.blue.bold('yarn preview'))
  console.log(chalk.blue('You can analyze your build by running:'), chalk.blue.bold('yarn analyze'))
  console.log()
})
