<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('owner')->default(0);
            $table->string('owner_email')->nullable();
            $table->integer('rate')->default(0);
            $table->integer('policy')->default(1);
            $table->integer('type')->default(1);
            $table->integer('acs')->default(0)->comment = "Number of accounts";
            $table->integer('city_id')->default(0);
            $table->timestamp('ends_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}
