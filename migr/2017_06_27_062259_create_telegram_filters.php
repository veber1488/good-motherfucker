<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelegramFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_filters', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id');
            $table->integer('category');
            $table->integer('type');
            $table->integer('typehouse');
            $table->integer('subtype');
            $table->integer('distr');
            $table->integer('square');
            $table->integer('square_area');
            $table->integer('view');
            $table->integer('state');
            $table->integer('floor');
            $table->integer('floors');
            $table->integer('appointment');
            $table->string('currency');
            $table->integer('city_id');
            $table->integer('studio');
            $table->integer('rooms_before');
            $table->integer('rooms_after');
            $table->float('price_start_EUR');
            $table->float('price_finish_EUR');
            $table->float('price_start_USD');
            $table->float('price_finish_USD');
            $table->float('price_start_UAH');
            $table->float('price_finish_UAH');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_filters');
    }
}
