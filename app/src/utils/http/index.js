import axios from 'axios'

const apiUrl = '/rest/'
const csrfToken = document.getElementsByName('csrf-token')[0]
  .getAttribute('content')

// allow use http client without Vue instance
export const http = axios.create({
  baseURL: apiUrl,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    common: {
      'X-CSRF-TOKEN': csrfToken
    }
  }
})

const xhr = {
  get (url, request) {
    return http.get(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  post (url, request) {
    return http.post(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  patch (url, request) {
    return http.patch(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  delete (url, request) {
    return http.delete(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  all (array) {
    return axios.all(array)
      .then(axios.spread((...response) => Promise.resolve(response)))
      .catch(axios.spread((...error) => Promise.reject(error)))
  }
}

/**
 * Helper method to set the header with the token
 */
export function setToken (token) {
  http.defaults.headers.common.Authorization = `Bearer ${token}`
}

// receive store and data by options
// https://vuejs.org/v2/guide/plugins.html
export default function install (Vue) {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty
  Object.defineProperty(Vue.prototype, '$http', {
    get () {
      return xhr
    }
  })
}
