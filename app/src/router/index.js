import Vue from 'vue'
import VueRouter from 'vue-router'

// Containers
import Full from '../containers/Full'

// Views
import Objects from '../components/Objects'
import Profile from '../components/Profile'
import Login from '../components/Login'
import Register from '../components/Register'
import ShareObjects from '../components/ShareObjects'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),

  routes: [
    {
      path: '/',
      component: Full,
      name: 'full',
      auth: true,
      children: [
        {
          path: 'objects/:category',
          component: Objects,
          name: 'objects',
          subRoutes: {
            '/:time': {
              component: Objects,
              name: 'time'
            }
          }
        },
        {
          path: 'share/:share_id',
          component: ShareObjects,
          name: 'share',
          auth: false
        },
        {
          path: 'profile',
          component: Profile,
          auth: true
        },
      ],
    },
    {
      path: '/login',
      component: Login,
      name: 'login'
    },
    {
      path: '/register',
      component: Register,
      name: 'register',
    },
  ],
})
