export default {
  translation: {
    'menu_cat_1': 'Sale',
    'menu_cat_2': 'Rent',
    'menu_sub_cat_1': 'Long Time',
    'menu_sub_cat_2': 'Short Time'
  }
}
