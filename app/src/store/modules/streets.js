import Vue from 'vue'
import {
  STREET_LOAD,
  STREET_UNLOAD,
  STREET_UPDATE,
  STREETS_PUSH,
  STREETS_PULL,
  STREETS_UPDATE,
} from '../mutation-types'

const
  state = {
    isLoaded: false,
    streets: [],
    street: null,
  },

  mutations = {
    [STREET_LOAD] (state, street) {
      Vue.set(state, 'street', street)
    },

    [STREET_UNLOAD] (state) {
      Vue.set(state, 'street', null)
    },

    [STREET_UPDATE] (state, street) {
      let idx = state.streets.findIndex(item => street.id === item.id)
      if (idx >= 0) {
        Vue.set(state.streets, idx, street)
        if (state.street && state.street.id === street.id) {
          Vue.set(state, 'street', street)
        }
      } else {
        Vue.set(state.streets, state.streets.length, street)
      }
    },

    [STREETS_PUSH] (state, street) {
      Vue.set(state.streets, state.streets.length, street)
    },

    [STREETS_PULL] (state, street) {
      state.streets.splice(state.streets.findIndex(item => street.id === item.id), 1)
      if (state.street && state.street.id === street.id) {
        Vue.set(state, 'street', null)
      }
    },

    [STREETS_UPDATE] (state, streets) {
      Vue.set(state, 'streets', streets)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    STREET_LOAD ({ commit }, street) {
      commit(STREET_LOAD, street)
    },
    STREET_UNLOAD ({ commit }) {
      commit(STREET_UNLOAD)
    },
    STREET_UPDATE ({ commit }, street) {
      commit(STREET_UPDATE, street)
    },
    STREETS_PUSH ({ commit }, street) {
      commit(STREETS_PUSH, street)
    },
    STREETS_PULL ({ commit }, street) {
      commit(STREETS_PULL, street)
    },
    STREETS_UPDATE ({ commit }, streets) {
      commit(STREETS_UPDATE, streets)
    },
  },

  getters = {
    streetsLoaded: state => state.streets.isLoaded,
    getStreets: state => state.streets.streets,
    getStreet: state => state.streets.street,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
