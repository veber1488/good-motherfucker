import cities from './cities'
import shares from './shares'
import streets from './streets'
import objects from './objects'
import reports from './reports'
import agencies from './agencies'
import districts from './districts'
import telegramUsers from './telegram_users'
import telegramFilters from './telegram_filters'

export default {
  cities,
  shares,
  streets,
  objects,
  reports,
  agencies,
  districts,
  telegramUsers,
  telegramFilters,
}
