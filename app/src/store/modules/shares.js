import Vue from 'vue'
import {
  SHARE_LOAD,
  SHARE_UNLOAD,
  SHARE_UPDATE,
  SHARES_PUSH,
  SHARES_PULL,
  SHARES_UPDATE,
} from '../mutation-types'

const
  state = {
    isLoaded: false,
    shares: [],
    share: null,
  },

  mutations = {
    [SHARE_LOAD] (state, share) {
      Vue.set(state, 'share', share)
    },

    [SHARE_UNLOAD] (state) {
      Vue.set(state, 'share', null)
    },

    [SHARE_UPDATE] (state, share) {
      let idx = state.shares.findIndex(item => share.id === item.id)
      if (idx >= 0) {
        Vue.set(state.shares, idx, share)
        if (state.share && state.share.id === share.id) {
          Vue.set(state, 'share', share)
        }
      } else {
        Vue.set(state.shares, state.shares.length, share)
      }
    },

    [SHARES_PUSH] (state, share) {
      Vue.set(state.shares, state.shares.length, share)
    },

    [SHARES_PULL] (state, share) {
      state.shares.splice(state.shares.findIndex(item => share.id === item.id), 1)
      if (state.share && state.share.id === share.id) {
        Vue.set(state, 'share', null)
      }
    },

    [SHARES_UPDATE] (state, shares) {
      Vue.set(state, 'shares', shares)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    SHARE_LOAD ({ commit }, share) {
      commit(SHARE_LOAD, share)
    },
    SHARE_UNLOAD ({ commit }) {
      commit(SHARE_UNLOAD)
    },
    SHARE_UPDATE ({ commit }, share) {
      commit(SHARE_UPDATE, share)
    },
    SHARES_PUSH ({ commit }, share) {
      commit(SHARES_PUSH, share)
    },
    SHARES_PULL ({ commit }, share) {
      commit(SHARES_PULL, share)
    },
    SHARES_UPDATE ({ commit }, shares) {
      commit(SHARES_UPDATE, shares)
    },
  },

  getters = {
    sharesLoaded: state => state.shares.isLoaded,
    getShares: state => state.shares.shares,
    getShare: state => state.shares.share,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
